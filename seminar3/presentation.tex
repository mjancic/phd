\documentclass[aspectratio=169,10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage[utf8]{inputenc}

\usepackage{booktabs}
\usepackage{physics}
\usepackage[scale=2]{ccicons}
\usepackage{amsmath,amsfonts,amssymb}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\p}{\emph{p}}
\renewcommand{\r}{\emph{r}}
\newcommand{\h}{\emph{h}}
\newcommand{\hp}{\emph{hp}}
\newcommand{\hr}{\emph{hr}}
\renewcommand{\b}{\boldsymbol}
\newcommand{\lap}{\nabla^2}
\newcommand{\einf}{e_{\infty}}
\newcommand{\n}{\b{n}}
\newcommand{\x}{\b{x}}
\newcommand{\w}{\b{w}}
\newcommand{\T}{\mathsf{T}}
\definecolor{mSybilaRed}{HTML}{990000}

\setbeamercolor{title separator}{
  fg=mSybilaRed
}

\setbeamercolor{background canvas}{bg=white}

\setbeamercolor{progress bar}{%
  fg=mSybilaRed,
  bg=mSybilaRed!90!black!30
}

\setbeamercolor{progress bar in section page}{
  use=progress bar,
  parent=progress bar
}

\setbeamercolor{alerted text}{%
  fg=mSybilaRed
}

\title{hp-adaptive method for solving partial differential equations}

\titlegraphic{\hfill\includegraphics[height=1.2cm]{sybila-logo/distComputeLogo.png}}
%\titlegraphic{\hfill\includegraphics[height=0.6cm]{sybila-logo/new.png}}
%\titlegraphic{\hfill\includegraphics[height=0.6cm]{sybila-logo/old.png}}
%\titlegraphic{\hfill\includegraphics[height=0.6cm]{sybila-logo/old-flat.png}}

\date{}
\author{Mitja Jančič}
\institute{Jožef Stefan Institute, Parallel and Distributed Systems  \\ International Postgraduate School Jožef Stefan}

%\title{Metropolis}
% \subtitle{A modern beamer theme for SYBILA}
% \date{\today}
\date{05. 10. 2022}
%\author{Matthias Vogelgesang}
% \institute{Center for modern beamer themes}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{sybila-logo/distComputeLogo.png}}

\setbeamertemplate{footline}
{
  \leavevmode
  \hbox{
  \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}

  \begin{beamercolorbox}[wd=.7\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}
    \usebeamerfont{author in head/foot}\insertshorttitle
  \end{beamercolorbox}

  \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}
    \insertframenumber{} / \inserttotalframenumber
  \end{beamercolorbox}
  }
}

\begin{document}

\maketitle

\begin{frame}{Table of contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

%
%
\section{Motivation}

\begin{frame}[fragile]{Why numerical solution?}
  \begin{columns}[T,onlytextwidth]
    \column{0.5\textwidth}
    Realistic problems do not have closed form solutions.
    \begin{align*}
      \div \vec{v}                                    & = 0,                                                       \\
      \pdv{\vec{v}}{t} + \vec{v} \cdot \grad{\vec{v}} & = -\grad p +\div(Ra \grad \vec{v}) - \vec{g}RaPr T_\Delta, \\
      \pdv{T}{t} + \vec{v} \cdot \grad{T}             & = \div( \grad
      T)
    \end{align*}

    \column{0.4\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{figures/sketches.png}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Numerical treatment}
  \begin{columns}[T,onlytextwidth]
    \column{0.5\textwidth}
    Numerical treatment is required:
    \begin{enumerate}
      \item Domain discretization
      \item Differential operator approximation
      \item PDE discretization
      \item Solve sparse linear system
    \end{enumerate}
    \metroset{block=fill}
    \begin{block}{Differential operator approximation}
      \begin{align*}
        (\mathcal L u)(\boldsymbol x_c)     & \approx \sum_{i=1}^n w_i u(\boldsymbol x_i)       \\
        \mathcal L \Big |_{\boldsymbol x_c} & = \boldsymbol w_{\mathcal L} (\boldsymbol x_c)^ T
      \end{align*}
    \end{block}
    \column{0.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.6\textwidth]{figures/mesh.png}
      \includegraphics[width=0.6\textwidth]{figures/grid.png}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Approximation basis}
  \begin{columns}[T,onlytextwidth]
    \column{0.48\textwidth}
    \metroset{block=fill}
    \begin{block}{RBF-FD}
      \begin{itemize}
        \item Polyharmonic splines augmented with monomials
        \item[+] Higher stability
        \item[-] Computationally complex
      \end{itemize}
    \end{block}
    \begin{itemize}
      \item Can operate on scattered nodes
      \item Control over the approximation order
    \end{itemize}
    \column{0.48\textwidth}
    % \metroset{block=fill}
    % \begin{block}{WLS}
    %   \begin{itemize}
    %     \item Monomials with Gaussian weights
    %     \item[+] Computationally cheap
    %     \item[-] Sometimes not stable
    %   \end{itemize}
    % \end{block}
    Polyharmonic splines
    \begin{equation*}
      f(r) = \begin{cases}r^k,       & k \text{ odd}  \\
             r^k\log r, & k \text{ even}\end{cases},
    \end{equation*}
    Augmentation with $N_p=\binom{m+d}{m}$ monomials $p$ with orders up to and including degree $m$,
    \begin{equation*}
      \begin{bmatrix}
        \b F    & \b P \\
        \b P^\T & \b 0
      \end{bmatrix}
      \begin{bmatrix}
        \b w \\
        \b \lambda
      \end{bmatrix}
      =
      \begin{bmatrix}
        \b\ell_f \\
        \b\ell_p
      \end{bmatrix}.
    \end{equation*}
  \end{columns}
  % Both:
  % \begin{itemize}
  %   \item work on scattered nodes
  %   \item allow for high order approximations
  % \end{itemize}
\end{frame}

\begin{frame}[fragile]{Approximation order}
  \begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/Ball_ConvergenceAll.pdf}
  \end{figure}
\end{frame}

%
%
\section{Problems of interest}
\begin{frame}[fragile]{Problems of interest}
  \begin{columns}[T,onlytextwidth]
    \column{0.39\textwidth}
    Mostly problems with:
    \begin{itemize}
      \item Strong sources
      \item Singularities
    \end{itemize}
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{figures/example_adaptive_solution.png}
    \end{figure}
    \column{0.59\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{figures/boussinesq_solution.png}
      \includegraphics[width=\textwidth]{figures/fwo_solution.png}
    \end{figure}
  \end{columns}
\end{frame}

%
%
\section{Optimized solution procedures}

\begin{frame}[fragile]{Optimized solution procedures}
  \begin{itemize}
    % \item Hybrid RBF-FD -- WLS approximation
    \item \p-refinement
    \item Adaptive \hp-refinement
          % \item[$\star$] Hybrid scattered-uniform discretizations
  \end{itemize}
\end{frame}

%
%
% \section*{Hybrid RBF-FD -- WLS}

% \begin{frame}{Hybrid RBF-FD -- WLS: Problem setup}
%   \begin{columns}[T,onlytextwidth]
%     \column{0.39\textwidth}
%     \begin{itemize}
%       \item Poisson problem with exponentially strong source in the domain
%             \begin{align*}
%               \grad^2 u(\boldsymbol x) & = f(\boldsymbol x)                                                 \\
%               f(\boldsymbol x)         & = \exp(-\alpha \left \| \boldsymbol x - \boldsymbol x_s \right \|)
%             \end{align*}
%       \item The division of nodes into RBF-FD and WLS sets has been done apriori
%     \end{itemize}
%     \column{0.59\textwidth}
%     \begin{figure}
%       \centering
%       \includegraphics[width=0.7\textwidth]{figures/approximation_engine.png}
%     \end{figure}
%   \end{columns}
% \end{frame}

% \begin{frame}{Hybrid RBF-FD -- WLS: Convergence rates}
%   \begin{figure}
%     \centering
%     \includegraphics[width=\textwidth]{figures/convergence_rates.png}
%   \end{figure}
% \end{frame}

% \begin{frame}{Hybrid RBF-FD -- WLS: Computational times}
%   \begin{columns}[T,onlytextwidth]
%     \column{0.39\textwidth}
%     Hybrid RBF-FD -- WLS method successfully:
%     \begin{itemize}
%       \item increased stability of the solution procedure and
%       \item decreased the computational complexity
%     \end{itemize}
%     \column{0.59\textwidth}
%     \begin{figure}
%       \centering
%       \includegraphics[width=0.7\textwidth]{figures/shape_computation_times.png}
%     \end{figure}
%   \end{columns}
% \end{frame}

% \begin{frame}{Hybrid RBF-FD -- WLS: Boussinesq's problem}
%   \begin{columns}[T,onlytextwidth]
%     \column{0.3\textwidth}
%     The problem is governed by the Cauchy-Navier equations
%     \begin{equation*}
%       (\lambda +\mu)\nabla (\nabla \cdot \b u)+\mu\nabla^2\b u = \b f
%     \end{equation*}
%     with a closed form solution.
%     \begin{table}
%       \caption{Comparison table for the solution of  Boussinesq's problem.}
%       \begin{center}
%         % \renewcommand\arraystretch{1.4}
%         \begin{tabular}{cccc} \hline
%           \multicolumn{1}{c}{Approximation} & \multicolumn{1}{c}{$\einf$} & \multicolumn{1}{c}{$t_{\text{shape}}$ [s]} & \multicolumn{1}{c}{$N_{\text{RBF-FD}} / N \cdot 100$}
%           \\ \hline
%           WLS                               & NaN                         & 4.74                                       & 0.00                                                  \\
%           RBF-FD                            & 9.48$\cdot 10^{-5}$         & 8.22                                       & 100.00                                                \\
%           hybrid                            & 2.37$\cdot 10^{-3}$         & 6.15                                       & 34.28                                                 \\ \hline
%         \end{tabular}
%         \label{tab:3d-data}
%       \end{center}
%     \end{table}
%     \column{0.6\textwidth}
%     \begin{figure}
%       \centering
%       \includegraphics[width=0.7\textwidth]{figures/contact.png}
%     \end{figure}
%   \end{columns}
% \end{frame}

%
%
\section*{\p-refined solution procedure}

\begin{frame}{Refinement methods}
  Refining methods are indispensable in problems where the solution error varies significantly throughout the computational domain.
  \underline{Refinement gravely improves accuracy of numerical method.}
  \begin{itemize}
    \item \p-refinement
    \item \h-refinement
    \item \hp-refinement
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/refinements.png}
  \end{figure}
\end{frame}

%
%
\begin{frame}{\p-refinement: Problem setup}
  \begin{columns}[T,onlytextwidth]
    \column{0.39\textwidth}
    Poisson problem with strong source in the domain
    \begin{align*}
      \lap u (\x)         & = f_{\text{lap}}(\x)                                        \\
      f_{\text{lap}} (\x) & = 3200\frac{25 \left \| 4\x - \b 2 \right \|^ 2}{f(\x)^3} -
      800\frac{d}{f(\x)^2}
    \end{align*}
    \column{0.59\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.7\textwidth]{figures/p-refinement.png}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{\p-refinement: Demonstration}
  \begin{columns}[T,onlytextwidth]
    \column{0.49\textwidth}
    Different approximation orders $m$ are used:
    \begin{equation*}
      m = \left\{\begin{matrix}
        6, & \left \| x_i - x_s \right \| \leq r_6     \\
        4, & r_6<\left \| x_i - x_s \right \| \leq r_4 \\
        2, & \text{otherwise.}
      \end{matrix}\right.
    \end{equation*}
    And three apriori prescribed approximation order distributions
    \begin{align*}
      c_1 & = \left \{r_6= 0, r_4 = \frac{1}{10}\right \},                      \\
      c_2 & = \left \{r_6= \frac{1}{10}, r_4 = \frac{1}{5}\right \} \text{ and} \\
      c_3 & = \left \{r_6= \frac{1}{5}, r_4 = \frac{2}{5}\right \}.
    \end{align*}
    \column{0.49\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.48\textwidth]{figures/c1.png}
      \includegraphics[width=0.48\textwidth]{figures/c2.png}
      \includegraphics[width=0.48\textwidth]{figures/c3.png}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{\p-refinement: Convergence rates}
  \begin{figure}
    \centering
    \includegraphics[width=0.49\textwidth]{figures/convergence.pdf}
    \includegraphics[width=0.49\textwidth]{figures/refined_convergence.pdf}
  \end{figure}
\end{frame}

\begin{frame}{\p-refinement: Computational times}
  \begin{columns}[T,onlytextwidth]
    \column{0.39\textwidth}
    Using \p-refinement we successfully improved convergence rates at \underline{a very small additional cost} to execution times.
    \column{0.59\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.9\textwidth]{figures/times.pdf}
    \end{figure}
  \end{columns}
\end{frame}

%
%
\section*{\hp-adaptive solution procedure}

\begin{frame}{\hp-adaptivity: Workflow}
  \begin{enumerate}
    \item \textbf{Solve} -- A numerical solution $\widehat{u}$ is obtained.
    \item \textbf{Estimate} -- An estimate of the spatial accuracy of the numerical solution is calculated using error indicators.
    \item \textbf{Mark} -- Depending on the error indicator values $\eta _i$, a marking strategy is used to mark the computational nodes for (de)refinement.
    \item \textbf{Refine} -- Refinement strategy is employed to define the amount of the (de)refinement.
  \end{enumerate}
  \begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/refinement_workflow.png}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Estimate module -- IMEX}
  \begin{columns}[T,onlytextwidth]
    \column{0.49\textwidth}
    Consider a problem of type
    \begin{equation*}
      \label{eq: general PDE}
      \mathcal L u = f_{RHS}.
    \end{equation*}
    The \underline{IM}plicit-\underline{EX}plicit error indicator:
    \begin{enumerate}
      \item Obtain implicit solution $u^{(im)}$ to governing problem using low-order approximations of $\mathcal L$, i.e.~$\mathcal L _{(im)}^{(lo)}$.
      \item Obtain high-order approximations of explicit operators $\mathcal L$, i.e.~$\mathcal L _{(ex)}^{(hi)}$
      \item Apply $\mathcal L _{(ex)}^{(hi)}$ to $u^{(im)}$ and obtain $f_{(ex)}$ in the process
      \item Compare $f_{RHS}$ and $f_{(ex)}$
    \end{enumerate}
    \column{0.49\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.6\textwidth]{figures/imex.png}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{\hp-adaptivity: Mark module}
  The modified Texas Three-Fold strategy for error indicator field $\eta$
  \begin{equation*}
    \begin{cases}
      \eta _i > \alpha \eta_{max},                          & \text{ refine}     \\
      \beta \eta_{max} \leq \eta _i \leq \alpha \eta_{max}, & \text{ do nothing} \\
      \eta _i < \beta \eta_{max},                           & \text{ derefine}
    \end{cases}.
  \end{equation*}
  \begin{columns}[T,onlytextwidth]
    \column{0.49\textwidth}
    \metroset{block=fill}
    \begin{exampleblock}{Advantage}
      Easy to understand and implement.
    \end{exampleblock}
    \column{0.49\textwidth}
    \metroset{block=fill}

    \begin{alertblock}{Problem}
      Does not lead to optimal results.
    \end{alertblock}
  \end{columns}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/indicator_sketch.pdf}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Refine module}
  Defining \underline{the amount} of (de)refinement.
  \begin{columns}[T,onlytextwidth]
    \column{0.49\textwidth}
    \h-refine:
    \begin{equation*}
      \label{eq:refinement}
      h_i^{new}(\b p) = \frac{h_i^{old}}{\frac{\eta_i - \alpha \eta _{max}}{\eta_{max} - \alpha\eta_{max}}\big(\lambda - 1\big) + 1}
    \end{equation*}
    \column{0.49\textwidth}
    \h-derefine:
    \begin{equation*}
      \label{eq:derefinement}
      h_i^{new}(\b p) = \frac{h_i^{old}}{\frac{\beta \eta _{max} - \eta_i}{\beta\eta_{max} - \eta_{min}}\big(\frac{1}{\vartheta} - 1\big) + 1},
    \end{equation*}
  \end{columns}
  \begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{figures/refinement_rules.png}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Brief study of free parameters}
  \begin{figure}
    \centering
    \includegraphics[width=0.95\textheight]{figures/refinement_demonstration.png}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Example problem}
  \begin{columns}[T,onlytextwidth]
    \column{0.55\textwidth}
    Poisson problem with exponentially strong source in the domain
    \begin{align*}
      \lap u (\x)   & = 2a e^{-a \left \| \x - \x_s \right \|^2}(2a\left \| \x - \x_s \right \| - d) & \text{in } \Omega,   \\
      u (\x)        & = e^{-a \left \| \x - \x_s \right \|^2}                                        & \text{on } \Gamma_d, \\
      \nabla u (\x) & = -2a(\x - \x_s)e^{-a \left \| \x - \x_s \right \|^2}                          & \text{on } \Gamma_n
    \end{align*}
    \begin{block}{Setup}
      \begin{itemize}
        \item RBF-FD
        \item PHS order $k=3$
        \item Monomial augmentation with $m\in \left \{2, 4, 6, 8 \right \}$
        \item IMEX with monomials $m\in \left \{4, 6, 8, 10 \right \}$
      \end{itemize}
    \end{block}
    \column{0.44\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.9\textwidth]{figures/example_adaptive_solution.png}
    \end{figure}
  \end{columns}
\end{frame}


\begin{frame}{\hp-adaptivity: Demonstration}
  \begin{figure}
    \centering
    \includegraphics[width=0.88\textheight]{figures/refinement_demonstration_2d.png}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Convergence rates -- IMEX}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/error_indicator_convergence.png}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Convergence rates -- comparrison}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/convergence_of_h_hp.png}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Fretting fatigue problem - problem setup}
  \begin{columns}[T,onlytextwidth]
    \column{0.45\textwidth}
    The problem is governed by the Cauchy-Navier equations
    \begin{equation*}
      (\lambda +\mu)\nabla (\nabla \cdot \b u)+\mu\nabla^2\b u = \b f
    \end{equation*}
    \begin{block}{Setup}
      \begin{itemize}
        \item RBF-FD
        \item PHS order $k=3$
        \item Monomial augmentation with $m\in \left \{2, 4, 6, 8 \right \}$
        \item IMEX with monomials $m\in \left \{4, 6, 8, 10 \right \}$
      \end{itemize}
    \end{block}
    \column{0.55\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.98\textwidth]{figures/FWO_scheme.pdf}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{\hp-adaptivity: Fretting fatigue problem -- Example solution}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/fwo_solution.png}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Fretting fatigue problem -- Surface traction}
  \begin{columns}[T,onlytextwidth]
    \column{0.3\textwidth}
    \begin{itemize}
      \item Surface traction $\sigma_{xx}$ under the contact
      \item Non-trivial local approximation order distribution
      \item Increased nodal density
      \item Good agreement with FEM solution
    \end{itemize}
    \column{0.7\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=\textheight]{figures/fwo_abaqus_seminar.png}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{\hp-adaptivity: Boussinesq's problem}
  \begin{columns}[T,onlytextwidth]
    \column{0.5\textwidth}
    The problem is governed by the Cauchy-Navier equations
    \begin{equation*}
      (\lambda +\mu)\nabla (\nabla \cdot \b u)+\mu\nabla^2\b u = \b f.
    \end{equation*}
    \begin{block}{Setup}
      \begin{itemize}
        \item RBF-FD
        \item PHS order $k=3$
        \item Monomial augmentation with $m\in \left \{2, 4, 6, 8 \right \}$
        \item IMEX with monomials $m\in \left \{4, 6, 8, 10 \right \}$
      \end{itemize}
    \end{block}
    \column{0.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.8\textwidth]{figures/boussinesq_sketch.pdf}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{\hp-adaptivity: Boussinesq problem -- Example solution}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/boussinesq_solution.png}
  \end{figure}
\end{frame}

\begin{frame}{\hp-adaptivity: Boussinesq problem -- von Mises stress}
  \begin{columns}[T,onlytextwidth]
    \column{0.3\textwidth}
    \begin{itemize}
      \item The von Mises stress $\sigma_{VMS}$ along body diagonal
      \item Non-trivial local approximation order distribution
      \item Increased nodal density
      \item Good agreement with closed form solution
      \item[\textbf{+}] Avoided fine-tunning with free parameters
    \end{itemize}
    \column{0.7\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.9\textheight]{figures/iterations_boussinesq.png}
    \end{figure}
  \end{columns}
\end{frame}

%
%
% \section{Computational complexity optimization}

% \begin{frame}[fragile]{Computational complexity optimization}
%   \begin{columns}[T,onlytextwidth]
%     \column{0.5\textwidth}
%     Heated cylinder in a square cavity. Governing equations
%     \begin{align*}
%       \div \vec{v}                                    & = 0,                                                       \\
%       \pdv{\vec{v}}{t} + \vec{v} \cdot \grad{\vec{v}} & = -\grad p +\div(Ra \grad \vec{v}) - \vec{g}RaPr T_\Delta, \\
%       \pdv{T}{t} + \vec{v} \cdot \grad{T}             & = \div( \grad
%       T)
%     \end{align*}

%     \begin{block}{Goal}
%       Reduce computational complexity with minimum cost to accuracy of numerical solution.
%     \end{block}

%     \column{0.4\textwidth}
%     \begin{figure}
%       \centering
%       \includegraphics[width=\textwidth]{figures/sketches.png}
%     \end{figure}
%   \end{columns}
% \end{frame}

% %
% %
% \section*{Hybrid scattered-uniform discretizations}

% \begin{frame}[fragile]{Hybrid scattered-uniform: Idea}
%   \begin{columns}[T,onlytextwidth]
%     \column{0.5\textwidth}
%     \metroset{block=fill}

%     \begin{block}{RBF-FD}
%       \begin{itemize}
%         \item [+] Stable on scattered nodes
%         \item [-] Computationally expensive (stencil size $n=13$)
%       \end{itemize}
%     \end{block}
%     \metroset{block=fill}

%     \begin{block}{COLLOCATION}
%       \begin{itemize}
%         \item [+] Unstable on scattered nodes
%         \item [-] Extremely cheap (stencil size $n=5$)
%       \end{itemize}
%     \end{block}

%     \begin{alertblock}{Idea}
%       Use scattered nodes only where required, i.e. in the neighborhood of irregular boundaries.
%     \end{alertblock}

%     \column{0.5\textwidth}
%     \begin{figure}
%       \centering
%       \includegraphics[width=0.95\textwidth]{figures/approx_distribution.png}
%     \end{figure}
%   \end{columns}
% \end{frame}

% \begin{frame}[fragile]{Hybrid scattered-uniform: Example solution}
%   \begin{figure}
%     \centering
%     \includegraphics[width=0.8\textwidth]{figures/natural_timesteps.png}
%   \end{figure}
% \end{frame}

% \begin{frame}[fragile]{Hybrid scattered-uniform: Nusselt number}
%   \begin{figure}
%     \centering
%     \includegraphics[width=0.95\textwidth]{figures/nusselt_natural.png}
%   \end{figure}
% \end{frame}

% \begin{frame}[fragile]{Hybrid scattered-uniform: Computational times}
%   \begin{figure}
%     \centering
%     \includegraphics[width=0.6\textwidth]{figures/natural_times.png}
%   \end{figure}
% \end{frame}


\section{Conclusions}

\begin{frame}{Summary}
  \begin{columns}[T,onlytextwidth]
    \column{0.5\textwidth}
    Presented:
    \begin{itemize}
      % \item Hybrid RBF-FD -- WLS approximation
      \item Conceptual \p-refinement
      \item Adaptive \hp-refinement
            % \item[$\star$] Hybrid scattered-uniform discretizations
    \end{itemize}
    \column{0.5\textwidth}
    % \begin{figure}
    %   \centering
    %   \includegraphics[width=0.95\textwidth]{figures/duck_solution.png}
    % \end{figure}
  \end{columns}
  Future work:
  \begin{itemize}
    % \item[$\star$] Hybrid scattered-uniform discretizations
    \item[$\star$] Different marking and refinement strategies in the \hp-adaptivity
    \item[$\star$] Different error indicators in the \hp-adaptivity
    \item[$\star$] \hp-adaptivity in the context of fluid flow problems
  \end{itemize}

\end{frame}

\begin{frame}[standout]
  Questions?
\end{frame}

\end{document}
