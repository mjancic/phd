#include <medusa/Medusa.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include <string>
#include <vector>
#include <algorithm>
#include <Eigen/SparseLU>

using namespace mm;

typedef Vec<double, 2> vec; // vector

int main(int argc, char *argv[]) {

    // Initialize timer
    Timer t;
    t.addCheckPoint("begin");

    // Domain definition
    BoxShape<vec> b(0, 1);
    const double dx = 0.2;

    std::cout << "Uniform grid" << std::endl;
    DomainDiscretization<vec> domainUniform = b.discretizeWithStep(dx);
    prn(domainUniform.size());

    std::cout << "Corner grid" << std::endl;
    auto fn = [=](const vec& p) {
        double h;
        h = sqrt(pow(p[0] - 1, 2) + pow(p[1], 2)) * 0.41;
        return std::max(h, 0.06);
    }; // lambda function on case you want to change density in domain

    DomainDiscretization<vec> domainCorner = b.discretizeBoundaryWithDensity(fn);
    GeneralFill<vec> fill;
    fill.seed(35);
    fill(domainCorner, fn);
    prn(domainCorner.size());

    // Domain definition
    BoxShape<vec> b2({1 - 2 * dx, 0}, {1, 2 * dx});
    const double dx2 = 0.5 * dx;

    std::cout << "Uniform grid two" << std::endl;
    DomainDiscretization<vec> domainUniform2 = b2.discretizeWithStep(dx2);
    domainUniform2 += domainUniform;
    prn(domainUniform2.size());

    DomainDiscretization<vec> domainSup = b.discretizeWithStep(dx2);
    prn(domainSup.size());
    // Support nodes
    t.addCheckPoint("support");
    std::cout << "find support" << std::endl;
    domainSup.findSupport(FindClosest(5));
    auto sup1 = domainSup.supports();
    domainSup.findSupport(FindClosest(10));
    auto sup2 = domainSup.supports();
    domainSup.findSupport(FindClosest(20));
    auto sup3 = domainSup.supports();

    t.addCheckPoint("postprocess");
    std::cout << "Saving to file" << std::endl;
    std::string output_file = "../results.h5";
    HDF file(output_file, HDF::DESTROY);
    file.close();

    // write to output file
    file.atomic().writeDouble2DArray("posUniform", domainUniform.positions());
    file.atomic().writeDouble2DArray("posCorner", domainCorner.positions());
    file.atomic().writeDouble2DArray("posUniform2", domainUniform2.positions());
    file.atomic().writeDouble2DArray("posUsup", domainSup.positions());
    file.atomic().writeInt2DArray("sup1", sup1);
    file.atomic().writeInt2DArray("sup2", sup2);
    file.atomic().writeInt2DArray("sup3", sup3);

    // Get sparse matrices
    Range<int> disc {10, 50, 100};
    for (int d : disc) {
        prn(d)
        BoxShape<vec> ball(0, 1);
        double s = 1.0/d;


        DomainDiscretization<vec> d1 = b.discretizeWithStep(s);

        Range<int> neu, dir;
        for (int i : d1.boundary()) {
            double x = d1.pos(i, 1);
            if (x < 0.5) dir.push_back(i);
            else neu.push_back(i);
        }


        d1.findSupport(FindClosest(15));
        int N = d1.size();
        prn("basis")
        Polyharmonic<double, 3> ph;
        RBFFD<decltype(ph), vec, ScaleToClosest> appr(ph, Monomials<vec>(2));
        // Compute shapes (only laplacian)
        prn("shapes")
        auto storage = d1.template computeShapes<sh::lap | sh::d1>(appr);
        prn("matrix")
        Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
        Eigen::VectorXd rhs(N);
        auto op = storage.implicitOperators(M, rhs);
        M.reserve(storage.supportSizes());

        prn("flling")
        for (int i : d1.interior()) {
            double sinProduct = 1.0;
            for (int j = 0; j < 2; j++) {
                double x = d1.pos(i, j);

                sinProduct *= std::sin(PI * x);
            }
            // Equation
            op.lap(i) = -2 * std::pow(PI, 2) * sinProduct;
        }

        // Apply boundary conditions
        for (int i : dir) {
            double sinProduct = 1.0;
            for (int j = 0; j < 2; j++) {
                double x = d1.pos(i, j);
                sinProduct *= std::sin(PI * x);
            }

            op.value(i) = sinProduct;
        }

        for (int i : neu) {
            op.neumann(i, d1.normal(i)) = 0.0;  // Neumann
//            double sinProduct = 1.0;
//            for (int j = 0; j < 2; j++) {
//                double x = d1.pos(i, j);
//                sinProduct *= std::sin(PI * x);
//            }

            //op.value(i) = sinProduct;
            // Equation holds also on the boundary, write it in the row corresponding to the ghost node.
//            op.neumann(i, d1.normal(i)) = -2 * std::pow(PI, 2) * sinProduct;
        }

        prn("compute")
        Eigen::SparseLU<decltype(M)> solver;
        solver.compute(M);
        prn("save")
        file.atomic().writeSparseMatrix("mat_" + std::to_string(d), M);
    }

    // End
    t.addCheckPoint("end");
    prn(t.duration("begin", "end"));
    std::cout << "Calculations saved to " << output_file << std::endl;
    return 0;
}
