using PyPlot
using LaTeXStrings

# Heaviside step function definition
H(x, k) = 1 ./ (1 .+ exp.(-2 .* k .* x));

h0 = 0.01;
h1 = 0.10;

function GetY(k, h0, h1, x)
            y = H(x, k);
            y = (y .- H(minimum(x), k)) ./ (H(maximum(x), k) .- H(minimum(x), k));
            y = h0 .+ (h1 - h0) .* y;
        end

# get points
N = 1000;
k_vals = 10 .^ (range(-2, 2, length = 10)); # logspaced parameters
x = range(-1, 1, length = N);
gr()
p = plot(0, lw = 0, title = "Heaviside step function approximations")
for i=1:length(k_vals)
    k = k_vals[i]
    println(i)
    if (k < 1e-5)
        y = ones(N);
    else
        y = GetY(k, h0, h1, x);
    end
    i += 1;
    plot!(p, x, y, lw = 1, label = L"\chi = ")
end
display(p)
# plotly() # Set the backend to gr() for LaTeX or use Plotly()
# plot(x, [y1, y2], title = "Step function", lw = 1, label = [L"H(x)" L"H_2(x)"])
xlabel!(L"x [m]")
ylabel!(L"h [m]")
