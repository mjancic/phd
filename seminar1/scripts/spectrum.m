close all
clear all
format compact
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IMPORT DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fn =  ['../analysis/results.h5'];
hinfo = h5info(fn);
s = [10 50 100];
f = setfig('a1', [600 300])
for i=1:length(s)
    subplot(1, 3, i)
    M = spconvert(h5read(fn, ['/mat_' num2str(s(i))]));
    spy(M, 'k')
    axis('equal')
    ticks = linspace(0, length(M) - 1, 5);
%     xticks(ticks)
%     yticks(ticks)
    xlim([min(ticks) max(ticks)])
    ylim([min(ticks) max(ticks)])
    xlabel('');
    ylabel('');
    grid on
    set(gca, 'GridLineStyle', '-')
    title(['$N = $' num2str(length(M))], 'Interpreter','latex')
end


% set(gcf, 'PaperUnits', 'inches');
% x_width=12 ;y_width=5;
% set(gcf, 'PaperPosition', [0 0 x_width y_width]); 
% saveas(f, '../images/spectrum.png');
exportf(f, '../images/spectrum.png', '-m4') 