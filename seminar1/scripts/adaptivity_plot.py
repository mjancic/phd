import h5py as h5
import matplotlib
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from scipy import spatial

sns.set()
sns.set_style("white")

# parameters
filePath = "../analysis/"
fileName = "results.h5"

# import data
data = h5.File(filePath + fileName, "r")
posU = data['posUniform'][:]
posC = data['posCorner'][:]
posU2 = data['posUniform2'][:]
posSup = data['posUsup'][:]
sup1 = data['sup1'][:]
sup2 = data['sup2'][:]
sup3 = data['sup3'][:]

d = {'x': posU[0], 'y': posU[1]}
data = pd.DataFrame(data=d)

# plot
fig, ax = plt.subplots(1, 3, figsize=(6, 2))
rect = patches.Rectangle((0,0),1, 1, fill=False, edgecolor='gray', linewidth=0.5)
ax[0].add_patch(rect)
ax[0].scatter(data["x"], data["y"], marker = ".", s = 50)
ax[0].axis('equal')
ax[0].axis("off")

d = {'x': posC[0], 'y': posC[1]}
data = pd.DataFrame(data=d)

rect = patches.Rectangle((0,0),1, 1, fill=False, edgecolor='gray', linewidth=0.5)
ax[1].add_patch(rect)
ax[1].scatter(data["x"], data["y"], marker = ".", s = 50)
ax[1].axis('equal')
ax[1].axis("off")

d = {'x': posU2[0], 'y': posU2[1]}
data = pd.DataFrame(data=d)

rect = patches.Rectangle((0,0),1, 1, fill=False, edgecolor='gray', linewidth=0.5)
ax[2].add_patch(rect)
ax[2].scatter(data["x"], data["y"], marker = ".", s = 50)
ax[2].axis('equal')
ax[2].axis("off")

fig.tight_layout()
fig.savefig('../images/adapts.pdf', format='PDF', DPI = 600)
# plot
fig, ax = plt.subplots(1, 3, figsize=(6, 2))

d = {'x': posSup[0], 'y': posSup[1]}
data = pd.DataFrame(data=d)
tree = spatial.KDTree([[x,y] for x,y in zip(data["x"], data["y"])])              # doctest: +SKIP
dist, idx = tree.query([0.5, 0.5], k = 1)                # doctest: +SKIP

rect = patches.Rectangle((0,0),1, 1, fill=False, edgecolor='gray', linewidth=0.5)
ax[0].add_patch(rect)
ax[0].scatter(data["x"], data["y"], marker = ".", s = 50)
sup = sup1[:, idx]
ax[0].scatter(data["x"][sup], data["y"][sup], marker = ".", c = 'r', s = 50)
ax[0].scatter(data["x"][idx], data["y"][idx], marker = ".", c = 'c', s = 50)
ax[0].axis('equal')
ax[0].axis("off")

rect = patches.Rectangle((0,0),1, 1, fill=False, edgecolor='gray', linewidth=0.5)
ax[1].add_patch(rect)
ax[1].scatter(data["x"], data["y"], marker = ".", s = 50)
sup = sup2[:, idx]
ax[1].scatter(data["x"][sup], data["y"][sup], marker = ".", c = 'r', s = 50)
ax[1].scatter(data["x"][idx], data["y"][idx], marker = ".", c = 'c', s = 50)
ax[1].axis('equal')
ax[1].axis("off")

rect = patches.Rectangle((0,0),1, 1, fill=False, edgecolor='gray', linewidth=0.5)
ax[2].add_patch(rect)
ax[2].scatter(data["x"], data["y"], marker = ".", s = 50)
sup = sup3[:, idx]
ax[2].scatter(data["x"][sup], data["y"][sup], marker = ".", c = 'r', s = 50)
ax[2].scatter(data["x"][idx], data["y"][idx], marker = ".", c = 'c', s = 50)
ax[2].axis('equal')
ax[2].axis("off")

fig.tight_layout()
fig.savefig('../images/adapts_p.pdf', format='PDF', DPI = 600)