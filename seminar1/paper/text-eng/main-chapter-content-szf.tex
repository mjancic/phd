%--------------------------------------------------------------------------------------------------
%
\section{Introduction}
%--------------------------------------------------------------------------------------------------
Human curiosity and desire to understand the natural phenomena have been
pushing the mankind forwards from the very beginning and continues to do so.
Commonly used approach to extend our understanding of nature is
computational
modelling, where complex natural phenomenon is first observed, then described
with mathematical models that are, in the final step, numerically solved by
computers.

A wide range of models are governed by partial differential equations (PDEs),
e.g.\ heat transfer, fluid flow and semiconductor models to name a few.
But obtaining the system of PDEs is only one part of the problem; the
second one is
solving that system. Tractable solutions to systems of PDEs are rare. Even
when they exist, they are most often not easily
obtained~\cite{morton_mayers_2005}. For some cases a closed form solution can
be obtained by using advanced
mathematical procedures and simplifications, which in the end make it
applicable only in certain scenarios. An alternative approach is to discretize
the problem at hand with the use of numerical methods, effectively transforming
the considered PDE into a system of algebraic
equations~\cite{plestenjak2015razsirjen, smith1985numerical,
	smith1985numerical}.
% Roughly speaking, numerical methods for solving PDEs can be grouped in methods
% that treat PDEs in a weak form or in a strong form formulation.
% Depending on our needs one form may be more favourable than the other. The
% strong
% form imposes continuity and differentiability requirements on the solutions
% while the weak form relaxes these requirements but only to a
% certain degree – meaning that a larger set of functions are possible solutions
% of the weak form. All solutions of the strong form satisfy the weak
% form but not vice-versa~\cite{morton_mayers_2005}.

Many methods for
numerical treatment of PDEs have been presented so far, ranging from weak form
Finite Element Method (FEM)~\cite{bathe1976numerical} to relatively simple
strong
form Finite Difference
Method (FDM)~\cite{smith1985numerical}. The FDM is often presented as the most
intuitive, easy to implement, and computationally effective method, but at
a
cost of limited usability, especially on irregular
domains. The matured FEM, on the
other hand, offers several advanced features
and has been accepted as standard numerical method for solving PDEs in weak
form by the community. However
an important downside of FEM is that it
requires mesh to operate. It is widely
accepted
that mesh generation is among the most cumbersome parts of the FEM analysis
and often requires human assistance, especially in complex 3D geometries. As an
alternative, in the 1970s meshless methods surfaced.

Meshless methods were introduced with the smoothed particle
hydrodynamics (SPH) by
Lucy~\cite{lucy1977numerical} and Gingold and
Monaghan~\cite{gingold1977smoothed}. SPH was used to solve astrophysical
problems.
Later applications to fluid
dynamics~\cite{monaghan1982particle,monaghan1988introduction,
	bonet2000correction} and solid
mechanics~\cite{allahdadi1993high} were also reported. The SPH was followed by
several FDM generalizations: the Finite Point Method~\cite{onate2001finite},
the Generalized Finite
Differences method~\cite{gavete2003improvements} and Radial Basis
Function-Generated Finite Differences (RBF-FD)~\cite{tolstykh2003using}. 

% While
% these are all based on a strong form formulation, methods that are
% based
% on weak form were also proposed soon after the emergence of SPH.
% The
% element-free Galerkin
% method~\cite{belytschko1994element} is considered to be the first
% meshless method based on a global weak form formulation. The Reproducing
% Kernel Particle method~\cite{liu1995reproducing}, which in contrast to the
% element-free Galerkin method has its origin in the wavelets, followed a year
% later. A new class of meshless methods that are based on \emph{local}
% weak forms was introduced in 1998 with the local
% Petrov-Galerkin~\cite{long2002meshless,
% atluri1998new, atluri2000meshless} method and the Moving
% Point Method~\cite{lohner2002finite, onate1998mesh, puso2008meshfree}.

Meshless methods have some very desireable features.
First, no mesh is required for operation, which
gravely simplifies the solution procedure without losing any generality
regarding the shape of considered domain, second, it has been reported that
the large deformations are handled
more robustly~\cite{han2005meshless}, and finally, the implementation
of
adaptive procedures~\cite{belytschko1996meshless} is simpler. However, in
general, the computational cost of meshless methods is higher than of mesh
based methods due to their generality - generation of shape functions
in weak form, or stencil weights computation in strong form context, is
transferred
from pre-process to the solution procedure~\cite{trobec2015parallel}.

In meshless methods domain discretization is simplified from meshing to
positioning of nodes. Although positioning of nodes is considered a much 
simpler task than meshing, it is still
not trivial. In the early years, many
authors used available mesh
generators to generate the discretization nodes and discarded the internodal
connectivity after the mesh had been generated~\cite{liu2009meshfree}. Such
procedure is conceptually flawed as the whole point of meshless methods it to 
avoid
meshing in all solution procedure steps. Additionally, some authors even
reported such approach failed
to generate nodal
distributions of required quality~\cite{shankar2015radial}. Consequently, 
several
algorithms dedicated to positioning of nodes for meshless discretization 
emerged, 
ranging from sphere
packing based algorithm~\cite{li2000point}, to front
advancing~\cite{lohner2004general}
and iterative
methods~\cite{kosec2018local}. In
2018 pure meshless algorithm based on Poisson disk
sampling~\cite{bridson2007fast} was introduced. In the same year the first
dimension-independent node generation algorithm that supports distributions
with spatially variable density~\cite{slak2019generation} was also proposed. In
the same paper, authors demonstrated the stability of RBF-FD on
nodes generated with the proposed algorithm, even for complex non-linear 
problems in 3D.

The appearance of dedicated discretization algorithm that supports the
variable nodal density and works well with meshless methods, specifically with
RBF-FD, soon led to adaptive solution of contact
problems~\cite{slak2019adaptive} that pushed a vivid research of
\emph{h-adaptive}
meshless methods~\cite{kosec2011h,slak2019adaptive, libre2008fast,duarte1996hp,
	liu2002adaptive, rabczuk2003adaptive, rabczuk2005adaptivity,
	haussler1998adaptive} one step further. Besides \emph{h-adaptivity}, also
meshless
\emph{r-adaptivity}~\cite{afshar2011node} and \emph{p-adaptivity}
~\cite{xin2000h,stevens2009use} have been reported.

Adaptive methods are indispensable in problems where the solution error varies
significantly throughout the computational domain. Typical examples of such
situations are crack propagation~\cite{rabczuk2007three} and solidification
simulation~\cite{kosec2014simulation}. In both examples the intensity of
simulation drastically increases near the crack or the
solidification front.
Solving such problems with adaptive methods gravely improves the performance
and
robustness of numerical method. Generally speaking, the
adaptive method tends to ensure relatively uniform error
distribution throughout the domain, thus, in many cases, assuring the existence
of numerical solution. Adaptive procedures, however, require one more
module for
effective
operation, an error indicator.

The first
introduction to error indicators in the scope of meshless methods has been
given by Durate and Oden~\cite{duarte1996hp}. The authors used a posteriori
error estimator and implemented it in \emph{hp-adaptive} method~\cite{krongauz1998efg}. The authors
of~\cite{chung2000adaptive}
suggested a very similar version to well-known \emph{ZZ} error
estimator~\cite{zienkiewicz1987simple} in the FEM. The basic idea of \emph{ZZ}
error
estimator is based on assumption that the error of the solution can be
approximated as the difference between the numerically obtained solution and a
recovered solution, i.e.\ a more accurate solution computed by appropriate
post-processing. Such error estimator is also used in~\cite{lee2003adaptive}.
An alternative class of error estimators, relying on the least squares
approximation residual~\cite{park2003posteriori,} is available for commonly
used
least squares-based meshless methods. Furthermore, for specific problems,
physical interpretations of the solution have also been
reported~\cite{davydov2011adaptive, kosec2011h} to help define domain areas of
high error probability.


% In this work we discuss high order dimension independent
% hp-adaptive meshless method. We will try to
% increase the stability and computational efficiency by controlling the order of
% the
% approximation method through the use of two different linear operator
% approximation methods, namely augmented RBF-FD~\cite{flyer2016role} and
% Generalised finite difference method (GFDM)~\cite{gavete2003improvements, 
% liszka1977finite}.

% The rest of the content is organized as follows: First a short and compact
% solution procedure for strong form mesh free problems is provided, followed by
% compact description of \emph{r, h} and \emph{p adaptivities} in the context of
% meshless methods.

%Over the last few years, meshless methods have become popular in various
%fields
%of science and
%engineering~\cite{fornberg2015solving, kosec2018local, budiana2020meshless,
%	zhang2018gpu} with recent uses in linear elasticity~\cite{slak2019refined},
%contact problems~\cite{slak2019adaptive},
%geosciences~\cite{fornberg2015primer}, fluid mechanics~\cite{kosec2018local},
%dynamic thermal rating of power lines~\cite{maksic2019cooling}, in the
%financial sector~\cite{milovanovic2018radial} and even with cases in higher
%dimensional spaces~\cite{jani2019analysis}.





%\subsection{Meshless methods}
%
%Meshless methods~\cite{tolstykh2003using,belytschko1996meshless} have some
%very
%desirable features. Among them no mesh is required for operation, which
%gravely simplifies the solution procedure without losing any generality
%regarding the shape of considered domain. It has been reported that problems
%with moving discontinuities (e.g.\ crack propagation~\cite{xin2000h})
%are treated with ease n. Large deformations~\cite{han2005meshless} are handled
%more robustly in the scope of meshless methods. Also the implementation of
%adaptive procedures r, h and p is
%simpler than in mesh based methods~\cite{belytschko1996meshless}, etc.
%
%However,
%there are some
%disadvantages too. For example, the shape functions are rational
%functions which require high-order integration scheme to be correctly
%computed and treatment of boundary conditions is not as straightforward as in
%mesh-based methods - they do not satisfy the Kronecker delta
%property~\cite{belytschko1996meshless}. In general, the computational cost of
%meshless methods is higher than one of finite element methods (FEM).
%
%From a historical point of view, meshless methods were introduced, with the
%objective of eliminating the difficulties associated with the mesh based
%methods, e.g.\ mesh generation. The first meshless method, with the roots from
%1980s, is considered to be
%the smooth particle hydrodynamics (SPH) method, by
%Lucy~\cite{lucy1977numerical} and Gingold and
%Monaghan~\cite{gingold1977smoothed}. The method was
%first used in the fields of
%astrophysics and fluid
%dynamics~\cite{monaghan1982particle,monaghan1988introduction,
%bonet2000correction}, later also in the field of solid
%mechanics~\cite{allahdadi1993high}.
%
%Roughly speaking, numerical methods can be grouped in methods that treat
%PDEs in a weak form or in a strong form formulation.
%Depending on our needs one may be more favourable than the other. The strong
%form imposes continuity and differentiability requirements on the solutions to
%the equation while the weak form relaxes these requirements but only to a
%certain degree – meaning that a larger set of functions are possible solutions
%of the weak form. All solutions of the strong form formulation satisfy the
%weak
%form but not vice-versa.
%
%While the SPH was based on a strong form formulation, methods that were based
%on
%weak form formulation were proposed soon after. The element-free Galerkin
%method~\cite{belytschko1994element} is considered to be one of the first
%meshless methods based on a global weak form formulation. The reproducing
%kernel particle method~\cite{liu1995reproducing}, which in contrast to the
%element-free Galerkin method has its origin in the wavelets, followed a year
%later.
%The h-p Cloud Methods~\cite{duarte1996h} also work on a global weak form
%formulation.
%
%Another class of meshless methods are methods that are based on \emph{local}
%weak forms which are generated on overlapping subdomains rather than using
%global
%weak form formulations. Most commonly used methods based on local weak form
%formulations is Petrov-Galerkin~\cite{long2002meshless, atluri1998new,
%atluri2000meshless}. Another, often applied in fluid mechanics, is the moving
%point method~\cite{lohner2002finite, onate1998mesh, puso2008meshfree}.
%
%Over the last few years, meshless methods have become popular in various
%fields
%of science and
%engineering~\cite{fornberg2015solving, kosec2018local, budiana2020meshless,
%zhang2018gpu} with recent uses in linear elasticity~\cite{slak2019refined},
%contact problems~\cite{slak2019adaptive},
%geosciences~\cite{fornberg2015primer}, fluid mechanics~\cite{kosec2018local},
%dynamic thermal rating of power lines~\cite{maksic2019cooling}, in the
%financial sector~\cite{milovanovic2018radial} and even with cases in higher
%dimensional spaces~\cite{jani2019analysis}.
%
%In this seminar we limit our research to strong form formulation
%of the PDEs and appropriate numerical methods, such as Radial Basis Function
%generated Finite Difference (RBF-FD) method~\cite{tolstykh2003using} and
%Generalised finite difference method (GFDM)~\cite{liszka1977finite,
%liszka1980finite, suchde2018meshfree} based on weighted least squares (WLS)
%approximation of the differential operators.
%
%\subsection{Domain discretization in meshless methods}
%When meshless methods were first developed, many authors used available mesh
%generators to generate the discretization nodes and discarded the internodal
%connectivity after the mesh had been generated~\cite{liu2009meshfree}. Such
%procedure is computationally wasteful and does not generalize to higher
%dimensional spaces. Some authors even reported it failed to generate node
%distributions of required quality~\cite{shankar2015radial}.
%
%A dedicated algorithm for node generation therefore had to be developed. In
%2018 first pure meshless algorithm based on Poisson disk
%sampling~\cite{bridson2007fast} was introduced. The same year the first
%dimension-independent node generation algorithm that supported distributions
%with spatially variable density~\cite{slak2019generation} appeared. In the
%same
%paper the authors also demonstrated the stability of RBF-FD on scattered
%nodes,
%even for complex non-linear problems in 3D. This is the node positioning
%algorithm we will be using in this seminar work.
%
%\subsection{Adaptive procedures in meshless methods}
%Solutions to many engineering or physics problems often significantly vary in
%magnitude throughout the problem domain. There can be different reasons why
%this happens. Sometimes its the complex geometrical or material properties,
%while other times the reason can be in the model itself – when some
%discontinuity or diverging area is present, e.g.\ contact
%problems~\cite{slak2019adaptive}. Such problems often introduce unstable
%systems which can be a challenging task for any numerical method.
%
%Although in some cases the areas with high error probability of numerical
%solution are known in advance, in general the error distribution is unknown
%beforehand. Adaptive techniques for solving PDEs are a standard way of dealing
%with this problem~\cite{davydov2011adaptive, slak2019adaptive}. The main goal
%of adaptive procedures is to increase the overall robustness of the numerical
%method by constructing an optimal node distribution or by choosing the best
%approximation method~\cite{nguyen2008meshless}. This allows us to attain the
%desired accuracy by using the least number of points as possible or by
%choosing
%the most appropriate approximation method. Either way, the fundamental task of
%an adaptive algorithm is to identify the problematic areas where more known
%data sets is needed and to avoid collecting unnecessary data in areas that do
%not require any modifications. Such areas in the domain are identified by
%using
%error estimators.
%
%The fact that the meshless methods operate on scattered nodes greatly
%simplifies the implementation of adaptive procedures. Adaptivity in meshless
%methods has already been addressed by many researchers. The different types of
%commonly used addaptive procedures are (i) \emph{h
%adaptivity}~\cite{kosec2011h,slak2019adaptive, libre2008fast,duarte1996hp,
%liu2002adaptive, rabczuk2003adaptive, rabczuk2005adaptivity,
%haussler1998adaptive} with respect to the reduction of the typical
%node-distances, (ii) \emph{r adaptivity}~\cite{afshar2011node} with respect to
%the redistribution of the nodes and (iii) \emph{p
%adaptivity}~\cite{xin2000h,stevens2009use} with respect to the control of the
%order of the approximation method.

%\subsection{Error estimators}
%The use of adaptive procedures is often not required on the whole domain, but
%rather only on certain problematic areas, e.g.\ contact problem.
%Such areas are identified with local error
%estimators, or at least an indicators of high error probability. The first
%introduction to error indicators in the scope of meshless methods has been
%given by Durate and Oden~\cite{duarte1996hp}. The authors used a posteriori
%error estimator and implemented it in hp-adaptive method. Later the a
%posteriori approximation error was used to adaptively refine
%derivatives~\cite{krongauz1998efg}. The authors of~\cite{chung2000adaptive}
%suggest a very similar version to well-known ZZ error
%estimator~\cite{zienkiewicz1987simple} in the FEM. The basic idea of the error
%approximation is based on assumption that the error of the solution can be
%approximated as the difference between the numerically obtained solution and a
%recovered solution, i.e.\ a more accurate solution computed by appropriate
%post-processing. Such error estimator is also used in~\cite{lee2003adaptive}.
%An alternative class of error estimators relying on the least squares
%approximation residual~\cite{park2003posteriori} is available for commonly
%used
%least squares-based meshless methods. Furthermore, for specific problems,
%physical interpretations of the solution have also been
%reported~\cite{davydov2011adaptive, kosec2011h} to help define domain areas of
%high error probability. Other error estimators, indicators and adaptive
%methods
%are proposed in~\cite{gavete2002procedure,
%gavete2001error,gavete2003posteriori,haussler1998adaptive,
%rabczuk2005adaptivity,lu2003adaptive}.

\section{Expected research results}
Significant variation in magnitude of numerical solutions is encountered in a
wide range of engineering or physics
models. Such solutions, if even obtained, are often accompanied with largely
non-uniform error
distribution throughout the domain making the numerical method unstable. 
Adaptive techniques for solving PDEs
are a standard way of dealing with the areas of high error estimator values.
Adaptivity in meshless methods
has already been addressed by many researchers. The most commonly used
adaptive procedures are:
\begin{itemize}
	\item \emph{h-adaptivity}~\cite{kosec2011h,slak2019adaptive,
		      libre2008fast,duarte1996hp, liu2002adaptive, rabczuk2003adaptive, 
		      rabczuk2005adaptivity, haussler1998adaptive} with respect to the reduction 
	      of the typical node-distances.
	\item \emph{r-adaptivity}~\cite{afshar2011node}: Adaptivity with respect to the redistribution of
	      the nodes is achieved through solution of an additional equation.
	\item \emph{p-adaptivity}: Adaptivity achieved by choosing the most
	      appropriate differential operator approximation method or by adjusting the
	      method order. The higher the order the more nodes are needed in the support
	      domain to assure the stability of approximation. It can also be achieved by
	      adding functions to the collocation set and keeping the number of nodes
	      constant.
	      P-adaptivity has been used by~\cite{xin2000h,stevens2009use} but is 
	      generally speaking more rare in meshless context. Therefore such adaptivity 
	      offers opportunity for further research. Not many researchers have shown 
	      much success in implementing the p-adaptivity in the context of meshless 
	      methods.
\end{itemize}

Effective numerical method for solving PDEs would be robust and computationally cheap. 
When choosing between two approximation methods, i.e.\ RBF-FD and WLS, it looks 
like we have two options: first, the robust RBF-FD method, and
second, computationally cheap WLS. Our wish is the combine the advantages of both 
and thus introduce a hybrid method, that is computationally cheaper than the RBF-FD alone 
and more robust when the WLS alone. 

Before such hybrid method can be developed and implemented, a thorough understanding of 
both underlying methods is needed. It is important to have the ability of 
prediciting when WLS can be used and when RBF-FD has to be applied in order to obtain 
a stable solution. The first benchmark towards a new hybrid method is therefore a
thorough analysis of both approximation methods in terms of stencil sizes, domain irregularity, highest augmented 
monomial degree, etc. A scan over several parameters is needed to evaluate the 
stability and time complexity of each of the child methods.

With the stability criteria determined, we than expect to derive a some sort of 
decision making tool 
to objectively define which of the two methods should be used in any computational 
node from the domain.

A step beyond the traditional adaptive procedures are automatic adaptivities,
where problematic regions are chosen automatically and then refined, until
certain error threshold is reached.

\section{Timeplan}
A rough timeplan to develop a hybrid numerical method is given below:
\begin{enumerate}[label=(\alph*)]
	\item \textbf{4 months:} A thorough analysis of RBF-FD and WLS in terms of
	      stability and computational complexity. This stage also depends on the availability 
	      of our servers. After the 3 months, we are expected to have a rough criteria
	      idea where and why WLS or RBF-FD is more favorable.
	\item \textbf{4 months:} Development of hybrid method. First, some sort of decision making
	      tool
	      is needed to automatically choose between the WLS and RBF-FD. Once that is given, 
	      the implementation of hybrid method can start. The hybrid
	      method will be implemented in C++ code as part of our Medusa library~\cite{medusa}.
	\item \textbf{2 months:} Application to a real world problem. We want to obtain
	      a numerical solution to a
	      realistic problem, e.g.\ fluid flow in 3D or thermoelasticity in 3D or 
	      anything equally challenging. The problem will be solved with all three 
	      linear differential operator approximation methods: RBF-FD, WLS and newly 
	      proposed hybrid method. We will hopefully observe that the chosen case 
	      is to complex to be solved with WLS alone, however using the hybrid method 
	      is computationally cheaper than the RBF-FD alone.
	\item \textbf{2-3 months:} As a result we plan to publish at least one
	      scientific paper in journal yet to be determined.
\end{enumerate}

This sums up to a total of 12-13 months plan.

\section{Planetary emergencies}

Many natural phenomena, ranging from melting of the polar ice caps, the global 
oceans dynamics, various weather systems, water transport, soil erosion and denudation, 
magma transport, and many more are described with partial differential equations (PDEs) 
that are in most practical cases unsolvable in closed form and therefore the numerical 
treatment is used instead. Meshless numerical methods for solving PDEs have become 
increasingly popular~\cite{nguyen2008meshless}, with recent uses in simulation of 
macrosegregation~\cite{ kosec2014simulation}, geosciences~\cite{fornberg2015primer}, 
fluid mechanics~\cite{kosec2018local}, dynamic thermal rating of power 
lines~\cite{maksic2019cooling}, advection-dominated 
problems~\cite{mavrivc2020equivalent,shankar2018hyperviscosity}, etc. 

The content of my thesis deals with the formulation of hybrid adaptive WLS-RBF-FD 
methods suitable for addressing problems with a pronounced difference in volatility 
of the solution within the considered domain and/or complex 3D geometries. We plan 
to address two problems covering both phenomena. First, the dynamic thermal rating 
of power lines and second, the simulation of face mask filtration. 

The increasing complexity of electrical power systems and increased demands for 
electrical power constantly pressure the transmission system operators to improve 
transmission capabilities~\cite{berizzi2004italian}, which are often limited with 
the maximal temperature of the power line that must not exceed a certain 
value~\cite{iglesias2014guide}. The placement of new transmission lines into the 
system, which is an immediate solution to the problem, is unfortunately limited 
due to the 
difficulty of acquiring new transmission line corridors, the extensive financial 
burden and vast societal consensus for environmental care. A possible approach to 
alleviate this problem is to integrate dynamic thermal rating (DTR)~\cite{iglesias2014guide}
in the transmission grid. This enables transmission system operators to better 
utilize existing infrastructure. Recently, such DTR system has been successfully 
integrated into Slovenian power grid. However, the physical model behind the DTR 
system has its limitations. It is well known that the most important factor in the 
thermal rating is convective cooling. This has been, to some degree, already discussed 
in~\cite{maksic2019cooling}, where authors addressed cooling due to the natural 
convection. The numerical simulation in~\cite{maksic2019cooling} is based solely 
on WLS, which proved to be unreliable. We will extend the findings presented 
in~\cite{maksic2019cooling}  by using the proposed hybrid approach to better capture 
fluid dynamics in the conductor proximity and consequently improve the DTR predictions.

The second example, the simulation of face mask filtration~\cite{tcharkhtchi2020overview}, 
is perhaps at the moment of COVID-19 pandemic situation of even greater importance 
as the majority of people worldwide have been asked to wear face masks almost 
overnight. Researchers have already simulated the effectiveness of wearing a face 
mask, i.e.\ face-mask contact~\cite{dbouk2020respiratory,peric2020analytical} where 
the mask itself was modelled with empirical relations that are accurate for a new 
mask. It is not clear how mask effectiveness changes with respiratory cycles due 
to humidity, penetrating dust, etc. A possible approach towards understanding the 
mask ageing is to simulate aerosol propagation through different types of filters. 
Such simulations are computationally extremely complex, primarily due to the highly 
complex 3D domains. Using RBF-FD in such cases will be computationally expensive, 
but the WLS alone lacks stability to obtain a reliable solution. Thus, a hybrid 
computationally stable and effective method is again very desirable. 

Generally speaking, any planetary emergency problem that can be modelled with a 
system of PDEs, and there is a myriad of such problems, falls under the scope of 
presented research of hybrid WLS-RBF FD method. 
