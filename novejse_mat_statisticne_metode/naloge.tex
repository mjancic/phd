\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage{enumitem}
\usepackage{afterpage}

\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Problem \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Novejše matematično statistične metode v naravoslovju 
in tehniki}
\newcommand{\hmwkDueDate}{\today}
\newcommand{\hmwkClass}{Predmet}
\newcommand{\hmwkClassTime}{}
\newcommand{\hmwkClassInstructor}{prof.\ dr.\ Matjaž Omladič}
\newcommand{\hmwkAuthorName}{\textbf{Mitja Jančič}}
\newcommand\blankpage{%
    \null
    \thispagestyle{empty}%
    \addtocounter{page}{-1}%
    \newpage}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{\hmwkDueDate}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor\ \hmwkClassTime}}
    \vspace{3in}
}

\author{\hmwkAuthorName}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}
\newcommand{\fX}[1]{f_{X}(#1)}
\newcommand{\fXY}[1]{f_{X|Y}(#1)}
\newcommand{\cov}[2]{\text{Cov}(#1, #2)}
\newcommand{\var}[1]{\text{Var}(#1)}

\begin{document}

\maketitle
\thispagestyle{empty}

\afterpage{\blankpage}
\pagebreak

\begin{homeworkProblem}
    Let $X$ be continuous random variable with the following PDF
    $$ \fX{x} = \left\{\begin{matrix}
        6x(1-x),& 0 \leq x \leq 1\\ 
        0, & otherwise 
       \end{matrix}\right.$$
    Suppose that we know
    $$Y|X=x \sim Geometric(x).$$
    Find the posterior density of $X$ given $Y=2$, $\fXY{x|2}$.
    \newline
    \newline
    \textbf{\underline{Solution}}
    \newline
    \newline
    We use Bayes
    $$
    \fXY{x|2} = \frac{P_{Y|X}(2|x)\fX{x}}{P_Y (2)}
    $$
    and 
    our knowledge about $x$
    $$
    P_{Y|X}(y|x)=x(1-x)^{y-1}.
    $$
    Thus we calculate
    \begin{align*}
        P_y(2) &= \int_{-\infty}^\infty P_{Y|X}(2|x)\fX x \dx =\\
        &= \int _0 ^1 x(1-x)6x(1-x)\dx = \\
        &= \frac 1 5.
    \end{align*}
    and obtain
    \begin{align*}
        \fXY{x|2}&=\frac{x(1-x)6x(1-x)}{\frac 1 5}=\\
        &=30 x^2 (1-x)^2.
    \end{align*}


\end{homeworkProblem}

\pagebreak

\begin{homeworkProblem}
    Let $X$ be continuous random variable with the following PDF
    $$ \fX{x} = \left\{\begin{matrix}
        3x^2,& 0 \leq x \leq 1\\ 
        0, & otherwise 
       \end{matrix}\right.$$
    Also Suppose that
    $$Y|X=x \sim Geometric(x).$$
    Find the MAP estimate of $X$ given $Y=5$.
    \newline
    \newline
    \textbf{\underline{Solution}}
    \newline
    \newline
    We know 
    $$Y|X=x \sim Geometric(x),$$
    so
    $$
    P_{Y|X}(y|x)=x(1-x)^{y-1}
    $$
    and therefore
    $$
    P_{Y|X}(5|x)=x(1-x)^{4}.
    $$
    We need to find the maximum of
    \begin{align*}
        P_{Y|X}(5|x) \fX x &= x(1-x)^4 3x^2=\\
        &=3x^3(1-x)^4
    \end{align*}
    in the interval $x\in [0, 1]$.
    We do that by differentiating
    $$
    \frac{\mathrm{d}}{\mathrm{d}x} \Big [ P_{Y|X}(5|x) \fX x \Big]= 3x^2(7x - 3)(x - 1)^3
    $$
    and finding all $x$ for which 
    $$
    3x^2(7x - 3)(x - 1)^3 = 0
    $$
    applies. Solving last equation for $x$ gives us
    \begin{align*}
        x_1 &= 0, \\
        x_2 &= \frac 3 7, \\
        x_3 &= 1.
    \end{align*}

    After chekcing the maximization criteria, we obtain the MAP estimate
    $$ \hat x _ {MAP} = x_2= \frac 3 7.$$

\end{homeworkProblem}
\pagebreak

\begin{homeworkProblem}[4]
    Let $X$ be continuous random variable with the following PDF
    $$ \fX{x} = \left\{\begin{matrix}
        2x^2 + \frac 1 3,& 0 \leq x \leq 1\\ 
        0, & otherwise 
       \end{matrix}\right.$$
    We also know that 
    $$ f_{Y|X}(y|x) = \left\{\begin{matrix}
        xy-\frac x 2+1,& 0 \leq x \leq 1\\ 
        0, & otherwise 
       \end{matrix}\right.$$

    Find the MMSE estimate of $X$, given $Y=y$ is observed.
    \newline
    \newline
    \textbf{\underline{Solution}}
    \newline
    \newline
    To find the posterior density $f_{X|Y}(x|y)$ we take
    $$
    f_{X|Y}(x|y) = \frac{f_{Y|X}(y|x)f_X(x)}{f_Y(y)}.
    $$

    Here $f_Y(y)$ is computed as
    \begin{align*}
        f_Y(y) &= \int _0 ^1 f_{Y|X}(y|x)f_X(x) \dx =\\
        &= \int _0 ^1 (2x^2 + \frac 1 3)(xy-\frac x 2+1)\dx = \\
        &=\frac{2}{3}(y + 1),
    \end{align*}
    thus
    $$
    f_{X|Y}(x|y) = \frac{3}{2}\frac{(2x^2 + \frac 1 3)(xy-\frac x 2+1)}{y + 1}.
    $$

    The MMSE estimate is given by
    \begin{align*}
        \hat x _ M &= E[X|Y = y]= \\
        &= \int _0 ^1x \fXY{x|y}\dx = \\
        &= \frac{3}{2(y + 1)}\int _0 ^1 x (2x^2 + \frac 1 3)(xy-\frac x 2+1) \dx = \\
        &= \frac{46y + 37}{90}.
    \end{align*}
\end{homeworkProblem}

\pagebreak

\begin{homeworkProblem}[7]
    Suppose that the signal $X\sim N(0,\sigma ^2_X)$ is transmitted over a 
    communication channel. Assume that the received signal is given by
    $$Y=X+W,$$
    where $W\sim N(0,\sigma ^2_W)$ is independent of $X$.

    \begin{enumerate}[label=\alph*.]
        \item Find the MMSE estimator of $X$ given $Y$, $(\hat X_M)$.
        \item Find the MSE of this estimator.
    \end{enumerate}

    \textbf{\underline{Solution}}
    \newline
    \newline
    We start by computing $\cov X Y $
    \begin{align*}
        \cov X Y &= \var X = \\
        &= \sigma ^2_X
    \end{align*}
    and
    \begin{align*}
        \rho &= \frac{\cov X Y}{\sigma_X \sigma_Y} = \\
        &=\frac{\sigma ^2_X}{\sigma _X \sqrt{\sigma ^2_X + \sigma ^2_W}}=\\
        &= \frac{\sigma _X}{\sqrt{\sigma ^2_X + \sigma ^2_W}}
    \end{align*}

    \begin{enumerate}[label=\alph*.]
        \item Find the MMSE estimator of $X$ given $Y$, $(\hat X_M)$.
            \begin{align*}
                \hat X _ M &= \mu_X + \rho \sigma_x\frac{Y - \mu_Y}{\sigma_Y}=\\
                &= \frac{\sigma ^2_X}{\sigma ^2_X + \sigma ^2_W} Y= \\
                &= AY.
            \end{align*}
        \item Find the MSE of this estimator.
        \begin{align*}
            E[(X - AY)^2] &= E[X^2 + 2XAY + Y^2]]= \\
            &= E[X^2] - 2AE[X]E[Y] + E[A^2Y^2]=\\
            &= \sigma ^2_X + \var{AY} + (AE[Y])^2= \\
            &= \sigma ^2_X + A^2 \sqrt{\sigma ^2_X + \sigma ^2_W}\\
            &= \sigma ^2_X + \frac{\sigma ^4_X}{\sigma ^2_X + \sigma ^2_W}
        \end{align*}
    \end{enumerate}
\end{homeworkProblem}
\pagebreak

\begin{homeworkProblem}[8]
    Let $X$ be an unobserved random variable with $EX=0$, $\var X = 5$. Assume 
    that we have observed $Y_1$ and $Y_2$ given by
    $$Y_1=2X+W_1,$$
    $$Y_2=X+W_2,$$
    where $EW_1=EW_2=0$, $\var{W_1}=2$, and $\var{W_2} = 5$. Assume that $W_1$, 
    $W_2$, and $X$ are independent random variables. Find the linear MMSE 
    estimator of $X$, given $Y_1$ and $Y_2$.
    \newline
    \newline
    \textbf{\underline{Solution}}
    \newline
    \newline
    The linear MMSE has the form
    $$\hat X_L = aY_1 + bY_2 + c.$$
    We have
    \begin{align*}
        E[\tilde X] &= aEY_1 + bEY_2 + c=\\
        &= c.
    \end{align*}
    Since $E[\tilde X] = 0$ also $c=0$. We note
    \begin{align*}
        \cov{\hat X_L}{Y_1} &= \cov{aY_1 + bY_2}{Y_1}=\\
        &= a\cov{2X + W_1}{2X + W_1} + b\cov{X + W_2}{2X + W_1}=\\
        &= a(4\cov X X + \cov{W_1}{W_1})+ 2b\cov X X =\\
        &= a(4\var X + \var{W_1})+ 2b\var X =\\
        &= 22a + 10b.
    \end{align*}

    Similary we find 
    \begin{align*}
        \cov{\hat X_L}{Y_2} &= \cov{aY_1 + bY_2}{Y_2}=\\
        &= a\cov{2X + W_1}{X + W_2} + b\cov{ X + W_2}{X + W_2}=\\
        &= 2a\cov X X + 2b (\cov X X + \cov{W_2}{W_2})=\\
        &= 2a\var X + 2b (\var X + \var{W_2})=\\
        &= 10a + 20b.
    \end{align*}

    We now conclude
    \begin{align*}
        22a +10b &= \var X = 5\\
        10a + 20b &=\var X =  5
    \end{align*}
    where solving for $a$ and $b$ we obtain 
    \begin{align*}
        a&= \frac{5}{34},\\
        b &= \frac{3}{17}.
    \end{align*}

    Therefore $$\hat X_L = \frac{5}{34}Y_1 + \frac{3}{17}Y_2.$$
\end{homeworkProblem}
\pagebreak

\begin{homeworkProblem}[11]
    Consider two random variables X and Y with the joint PMF given by the table below.
    \begin{equation*}
        \begin{matrix}
            & Y = 0 & Y = 1\\ 
           X = 0 & \frac 1 7 & \frac 3 7\\ 
           X = 1 & \frac 3 7& 0 
           \end{matrix}
    \end{equation*}

    \begin{enumerate}[label=\alph*.]
        \item Find the MMSE estimator of $X$ given $Y$, $(\hat X_L)$.
        \item Find the MSE of this estimator of $X$ given $Y$, $(\hat X_M)$.
        \item Find the MSE of $\hat X_M$.
    \end{enumerate}

    \textbf{\underline{Solution}}
    \newline
    \newline
    From the table we see
    \begin{align*}
        P_X(0) &= \frac 1 7 + \frac 3 7 = \frac 4 7,\\
        P_X(1) &= \frac 3 7 + 0 = \frac 3 7,\\
        P_Y(0) &= \frac 1 7 + \frac 3 7 = \frac 4 7,\\
        P_Y(1) &= \frac 3 7 + 0 = \frac 3 7.
    \end{align*}

    Thus the marginal distributions of $X$ and $Y$ are both $Bernoulli(\frac 3 7)$. 
    Therefore we have
    \begin{align*}
        EX &= EY = \frac 3 7\\
        \var X &= \var Y = \frac 3 7 \cdot \frac 4 7 = \frac{12}{49}.
    \end{align*}
    \begin{enumerate}[label=\alph*.]
        \item The MMSE estimator of $X$ given $Y$ is computed via $\cov X Y$. 
            We have $$EXY = \sum x_iy_jP_{XY}(x, y) = 0,$$
            therefore
            $$ \cov X Y = EXY - EX EY = -\frac{9}{49}.$$
            The MMSE estimator is then given as
            \begin{align*}
                \hat X_L &= \frac{\cov X Y}{\var Y}(Y - EY) + EX = \\
                &=\frac{-9/49}{12/49}(Y - \frac 3 7) + \frac 3 7\\
                &= -\frac 3 4 (Y - \frac 3 7) + \frac 3 7
            \end{align*}
        \item To find the MMSE estimator we have 
            $$ P_{X|Y} (0|0) = \frac{P_{XY}(0,0)}{P_Y(0)}=\frac{1 / 7}{4 / 7} = \frac 1 4.$$
            Thus
            $$ P_{X|Y}(1|0) = 1 - \frac 1 4  = \frac 3 4$$
            and we can similarly find
            \begin{align*}
                P_{X|Y}(0|1) &= 1,\\
                P_{X|Y}(1|1) &= 0.\\
            \end{align*}

            The MMSE estimator is then given as 
            $$ \hat X_M = E[X|Y],$$
            where
            \begin{align*}
                E[X|Y = 0] &= \frac 3 4\\
                E[X|Y = 1] &= 0. \\
            \end{align*}
        \item The MSE of $\hat X_M$ can be obtained as
            \begin{align*}
                MSE &= (1 - \rho(X, Y)^2)\var X=\\
                &= \Big( 1 - \frac{\cov X Y}{\var X \var Y}\Big )\var X= \\
                &= \frac{3}{28}.
            \end{align*}
    \end{enumerate}
\end{homeworkProblem}
\pagebreak


\begin{homeworkProblem}[15]
    A monitoring system is in charge of detecting malfunctioning machinery in a 
    facility. There are two hypotheses to choose from:
    \begin{itemize}[label={}]
        \item $H_0$: There is not a malfunction,
        \item $H_1$: There is a malfunction.
    \end{itemize}
    The system notifies a maintenance team if it accepts $H_1$. Suppose that, 
    after processing the data, we obtain $P(H_1|y)=0.10$. Also, assume that the 
    cost of missing a malfunction is 30 times the cost of a false alarm. Should 
    the system alert a maintenance team (accept $H_1$)?
    \newline
    \newline
    \textbf{\underline{Solution}}
    \newline
    \newline
    Note
    $$P(H_0|y) = 1 - P(H_1|y)=0.9.$$
    The posterior risk of accepting $H_1$ is
    $$
    P(H_0|y)C_{10} = 0.9C_{10}.
    $$
    We know $C_{01} = 30 C_{10}$, so the posterior risk of accepting $H_0$ is 
    \begin{align*}
        P(H_1|y)C_{01} = 0.3C_{10}.
    \end{align*}

    We now check if 
    $$ P(H_0|y)C_{10} \geq P(H_1|y)C_{01}.$$

    Here 
    $$P(H_0|y)C_{10} = 0.9C_{10}$$
    and
    $$P(H_1|y)C_{10} = 0.3C_{01}$$
    therefore we accept $H_0$.
\end{homeworkProblem}
\pagebreak

\begin{homeworkProblem}[16]
    Let $X$ and $Y$ be jointly normal and $X\sim N(2,1)$, $Y\sim N(1,5)$, and 
    $\rho(X,Y)=\frac 1 4$. Find a 90\% credible interval for $X$, given $Y=1$ is 
    observed.
    \newline
    \newline
    \textbf{\underline{Solution}}
    \newline
    \newline
    \begin{align*}
        E[X|Y=y] &= \mu_X + \rho \sigma_X \frac{Y - \mu_Y}{\sigma_Y} = 2\\
        Var(X|Y = y) &= (1-\rho^2) \sigma^2 _X = \frac{15}{16}
    \end{align*}

    Here $\alpha = 0.1$ so we need an interval $[a, b]$ for which 
    $$
    P(a \leq X \leq b | Y = 1) = 0.9.
    $$
    We choose symmetric interval, that is
    $$ \Big [ 2-c, 2+c \Big ].$$
    Thus we need to have 
    \begin{align*}
        P(2 - c \leq X \leq 2 + c | Y= 1)&= \Phi\Big(\frac{c}{\sqrt{15/16}}\Big) - \Phi\Big(\frac{-c}{\sqrt{15/16}}\Big)=\\
        &= 2 \Phi\Big(\frac{c}{\sqrt{15/16}}\Big) -1 = 0.9.
    \end{align*}

    Solving for $c$ we obtain
    $$
    c = \sqrt{\frac{15}{16}}\Phi^{-1} (0.95) \approx \sqrt{\frac{15}{16}} 1.645 = 1.6
    $$
    and finally the interval is
    $$
    \Big [ 0.4, 3.6 \Big ].
    $$
\end{homeworkProblem}
\end{document}